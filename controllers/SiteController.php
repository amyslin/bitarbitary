<?php

namespace app\controllers;

use yii\data\ArrayDataProvider;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\Json;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionChains() {
		$models = \app\models\BitChain::find()->with(['chainItems', 'chainItems.market'])->asArray()->all();
		$client = new \artdevision\bittrex\Client('2a7f2e4e4ad24cd09ca2f314bfb043fe', '55d2180c13c041d5acccbe332246611a');

		$dealAmount = 100;

		return $this->render('chains', [
			'provider' => 	new ArrayDataProvider([
				'allModels' => $models,
				'pagination' => [
					'pageSize' => 50,
				]
			]),
			'dealAmount' => $dealAmount,
		]);

		return print_r($models, true);

    }

    public function actionTest() {
    	$client = new \artdevision\bittrex\Client('2a7f2e4e4ad24cd09ca2f314bfb043fe', '55d2180c13c041d5acccbe332246611a');
    	$response = $client->getPublic(\artdevision\bittrex\Client::PUBLIC_CURRENCIES);

    	if ((int) $response->getStatusCode() == 200) {
    		$res = \yii\helpers\Json::decode((string) $response->getBody());
    		foreach($res as $item) {

    		}
    		    		echo \yii\helpers\VarDumper::dumpAsString($res);
    		echo \yii\helpers\VarDumper::dumpAsString($response->curl_info);
    	}
		return print_r([], true);
    }
    
    public function actionBinance() {
    	$client = new \artdevision\binance\Client('VQ97JJY7KX0Ar7gjOfMFZhxs1XF96KbjxOxIQ21whrMAgjPFTeloLegF4pNfMYXm', 'dwi1edS3sz9p1OpM00IsVfJWbOd8rXEGwi3RAWXe6Ppylu0SLlsallIeKmCyTr1R', [], 'v1');
    	
    	$timeResponse = $client->getPublic('v1/time');
    	$data = Json::decode((string) $timeResponse->getBody(), true);
    	
    	print_r($data['serverTime']);
    	
    	$response = $client->getSigned(\artdevision\binance\Client::SIGNED_ACCOUNT, ['timestamp'=>$data['serverTime']]);
    	
    	print_r(Json::decode((string) $response->getBody(), true));
    }
}
