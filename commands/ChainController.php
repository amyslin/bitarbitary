<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use Psr\Http\Message\ResponseInterface;
use artdevision\bittrex\Client;

class ChainController extends Controller {
	protected $_start_amount = 5;
	protected $_tergetProfit = 0.20;
	protected $_comissionFee = 0.0025;
	protected $_totalBalance  = 100;
	protected $_summaries = [];

	protected $_chainItems;

	public function actionStart() {
$sql = <<<SQL
SELECT
  bs.chain_id,
  MAX(bs.created_at) created_at,
  COUNT(bs.id) stat_count,
  AVG(bs.profit) avg_profit,
  MIN(bs.profit) min_profit,
  MAX(bs.profit) max_profit
 FROM bit_signals_stat bs
LEFT JOIN bit_chain bc ON bc.id = bs.chain_id
WHERE bs.created_at >= DATE_ADD(NOW(), INTERVAL -5 SECOND)
  #AND bs.chain_id IN (11, 13, 27)
  AND bs.profit >= :profit
GROUP BY bs.chain_id
HAVING COUNT(bs.id) > 3
ORDER BY max_profit DESC
LIMIT 1;
SQL;

		$run = true;
		$test = true;
		while($run) {
			$this->_summaries = [];
			$this->_summaries['amount_in'] = $this->_start_amount;
			$this->_summaries['amount_out'] = 0;
			sleep(1);
				$stat = \Yii::$app->db->createCommand($sql)->bindValue(':profit', $this->_tergetProfit)->queryOne();
				if (!empty($stat)) {
// 					$run = false;
					$chains = \app\models\BitChainItem::find()
						->where(['=', 'chain_id', $stat['chain_id']])
						->with('market')
						->orderBy('order', 'ask')
						->asArray()
						->all();
					
					$wallets = \app\models\BitWallet::find()
								->where(['=', 'exchange_id', 1])
								->select(['symbol', 'balance'])
								->asArray()
								->all();
					
					$balances = [];
					foreach ($wallets as $key => $wallet) {
						$balances[$wallet['symbol']] = $wallet['balance'];
					}

					if (empty($chains)) {
						$run = false;
						return;
					}
					$this->_chainItems = [];
					$break = false;
					foreach($chains as $key => $chain) {
						if ($test)
							print_r($stat['chain_id']);
						if ($key == 0)
							$coins = $this->_start_amount/(1+$this->_comissionFee);
						else
							$coins = ($chains[$key-1]['operation'] == 'BUY') ? 
										$this->_chainItems[$key-1]['quantity']/(1+$this->_comissionFee) : 
										$this->_chainItems[$key-1]['total']/(1+$this->_comissionFee);
						
						$symbol = ($chain['operation'] == 'BUY') ? $chain['market']['base_currency'] : $chain['market']['market_currency'];
						 
						
						$break = (isset($balances[$symbol])) ? (boolean) ($coins > $balances[$symbol]) : true; 
						
// 						$rate = round(($chain['operation'] == 'BUY') ?
// 									$chain['market']['last'] - ($chain['market']['last'] * $this->_comissionFee) :
// 									$chain['market']['last'] + ($chain['market']['last'] * $this->_comissionFee), 8);
						// Надо считать 
// 						$rate = round( 
// 								($chain['operation'] == 'BUY') ? 
// 								$chain['market']['last']/(1+$this->_comissionFee) : 
// 								$chain['market']['last'] + ($chain['market']['last'] * $this->_comissionFee), 
// 						8);
						//Продаем по рынку
						$rate = ($chain['operation'] == 'BUY') ? $chain['market']['last'] /*+ ($chain['market']['last'] * 0.0001) */: $chain['market']['bid'] /*- ($chain['market']['bid'] * 0.0001) */;

						$quantitry = round(($chain['operation'] == 'BUY') ? ($coins / $rate) : $coins, 8);
						
						

						$total = ($chain['operation'] == 'BUY') ? $coins : $coins * $rate;
						$total = round(
								($chain['operation'] == 'BUY')? 
								$total + ($total * $this->_comissionFee):
								$total/(1+$this->_comissionFee)
								, 8);

						$comission = $total * $this->_comissionFee;

						$x = (($quantitry + $comission)/$coins) + $rate;
						$rate_2 = ($quantitry + $comission) / $coins;
// 						$rate_2 = ($chain['operation'] == 'BUY') ? round(($quantitry + $comission) / $coins,8) : round(($quantitry - $comission) / $coins,8);

						$this->_chainItems[] = [
							'market' => $chain['market']['name'],
							'order' => $chain['operation'],
							'last' => $chain['market']['last'],
							'bid' => $chain['market']['bid'],
							'ask' => $chain['market']['ask'],
							'quantity' => number_format($quantitry, 8, '.', ''),
							'total' => number_format($total, 8, '.', ''),
							'rate' => number_format($rate, 8, '.', ''),
							'comission' => number_format($comission, 8, '.', ''),
							'closed' => false,
							'order_uid' =>null,
// 							'x' => number_format($x, 8, '.', ''),
// 							'rate_2' => number_format($rate_2, 8, '.', ''),
						];
						
					}
					if ($test)
						print_r($this->_chainItems);
					$last = $this->_chainItems[count($this->_chainItems) - 1];
					$this->_summaries['amount_out'] = $last['total'];
					$this->_summaries['profit'] = $this->_summaries['amount_out'] - $this->_summaries['amount_in'];
					$this->_summaries['percent'] = 100/($this->_summaries['amount_in']/$this->_summaries['profit']);

// 					print_r($this->_chainItems);

					print_r($this->_summaries);

// 					$run = false;
					
					if ($this->_summaries['percent'] > 0.1 && !$break && !$test) {
						$this->_totalBalance += $this->_summaries['profit'];
						print_r(['totalBalance' => $this->_totalBalance]);


						$client = new \artdevision\bittrex\Client('2a7f2e4e4ad24cd09ca2f314bfb043fe', '55d2180c13c041d5acccbe332246611a');

						foreach($this->_chainItems as $key => &$chain) {

							print_r($chain);
// 							if ($key == 0)
// 								continue;
							$method = ($chain['order'] == 'BUY') ? \artdevision\bittrex\Client::MARKET_BUYLIMIT : \artdevision\bittrex\Client::MARKET_SELLLIMIT;
							$result = $client->getMarket($method, [
								'market' => $chain['market'],
								'quantity' => $chain['quantity'],
								'rate' => $chain['rate']
							]);
							if ((int) $result->getStatusCode() == 200) {
								$res = \yii\helpers\Json::decode((string) $result->getBody());
								if ($res['success'] == true) {
									$chain['order_uid'] = $res['result']['uuid'];
// 									while(!$chain['closed']) {
// 										sleep(1);
// 										$response = $client->getAccount(\artdevision\bittrex\Client::ACCOUNT_ORDER, ['uuid' => $chain['order_uid']]);
// 										if ((int) $response->getStatusCode() == 200) {
// 											$data = \yii\helpers\Json::decode((string) $response->getBody());
// 											if ($data['success'] == true && !empty($data['result']['Closed']))
// 												$chain['closed'] = true;
// 											print_r($data);
// 										}


// 									}
								}
								else
									print_r($res);
							}
						}

// 						$run = false;
					}

				}
		}
	}
}