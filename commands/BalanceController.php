<?php

namespace app\commands;

use yii\console\Controller;
use Psr\Http\Message\ResponseInterface;
use artdevision\bittrex\Client;

class BalanceController extends Controller {
	public function actionIndex() {
		$client = new \artdevision\bittrex\Client('2a7f2e4e4ad24cd09ca2f314bfb043fe', '55d2180c13c041d5acccbe332246611a');
		$method = \artdevision\bittrex\Client::ACCOUNT_BALANCES;
		
		while(true) {
			$response = $client->getAccount($method);
			
	// 		print_r($response->getStatusCode());
			$wallets = \app\models\BitWallet::find()
						->where(['=', 'exchange_id', 1])
						->select(['symbol'])
						->asArray(true)
						->all();
			
			foreach ($wallets as $key => &$wallet)
				$wallet = $wallet['symbol'];
	// 			print_r($wallets);
			if ((int) $response->getStatusCode() == 200) {
				$data = \yii\helpers\Json::decode((string) $response->getBody());
	// 			print_r($data);
				if ($data['success'] == true)
					foreach($data['result'] as $item) {
						if (in_array($item['Currency'], $wallets)) {
							\app\models\BitWallet::updateAll([
									'balance' => $item['Available'],
							], [ 'and',
								['=', 'symbol', $item['Currency']],	
								['=', 'exchange_id', 1]	
							]);
							
// 							$command = \Yii::$app->db->createCommand();
// 							$command->attachBehaviors([\kozhemin\dbHelper\InsertUpdate::className()]);
// 							$command->insertIgnore();
							
						}
					}
				\app\models\BitWallet::updateHistory();
				\app\models\BitWallet::updateSummaries();
					
			}
			
			
			
		}
		
	}

}