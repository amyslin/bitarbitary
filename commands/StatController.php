<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;

class StatController extends Controller {
	public function actionIndex() {
// 	$sql = <<<SQL
// 	TRUNCATE TABLE bit_signals_stat;
// SQL;
// 	\Yii::$app->db->createCommand($sql)->execute();
	
$sql = <<<SQL
SELECT
    bc.id AS 'chain_id',
    bc.name,
    @amount:=100/(1+0.0025) AS 'amount_in',
    @r1:=
      CASE WHEN step1.operation = 'BUY' THEN
        CAST(@amount/m1.last AS DECIMAL(28,8)) 
      ELSE 
        CAST(@amount*m1.bid AS DECIMAL(28,8)) 
      END AS 'btc_val',
    @r2:=
      CASE WHEN step2.operation = 'BUY' THEN
        CAST((@r1/(1+0.0025))/m2.last AS DECIMAL(28,8))
      ELSE
        CAST((@r1/(1+0.0025))*m2.bid AS DECIMAL(28,8))
      END AS 'cur_res',
    @r3:=
      CASE WHEN step3.operation = 'BUY' THEN
        CAST((@r2/(1+0.0025))/m3.last AS DECIMAL(28,8)) 
      ELSE
        CAST((@r2/(1+0.0025))*m3.bid AS DECIMAL(28,8)) 
    END AS 'amount_out',
    CAST(@r3 - 100 AS DECIMAL(28,8)) AS 'profit',
    m1.last AS 'm1_last',
    m2.last AS 'm2_last',
    m3.last AS 'm3_last'

  FROM bit_chain bc
  LEFT JOIN bit_chain_item step1 ON bc.id = step1.chain_id AND step1.order = 1
  LEFT JOIN bit_chain_item step2 ON bc.id = step2.chain_id AND step2.order = 2
  LEFT JOIN bit_chain_item step3 ON bc.id = step3.chain_id AND step3.order = 3
  LEFT JOIN bit_market m1 ON step1.market_id = m1.id
  LEFT JOIN bit_market m2 ON step2.market_id = m2.id
  LEFT JOIN bit_market m3 ON step3.market_id = m3.id
  #WHERE CAST(@r3 - @amount AS DECIMAL(28,8)) > 0
  #WHERE bc.id < 18 AND bc.id NOT IN(14)
  ORDER BY CAST(@r3 - 100 AS DECIMAL(28,8)) DESC;
SQL;

	while(true) {
		sleep(1);
			$data = \Yii::$app->db->createCommand($sql)->queryAll();

			foreach ($data as $item) {
				if ($item['profit'] > 0) {
					$model = new \app\models\BitStat();
					$model->setAttributes($item);
					$model->save(false);
				}
			}

		}
	}
}