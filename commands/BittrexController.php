<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use Psr\Http\Message\ResponseInterface;
use artdevision\bittrex\Client;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BittrexController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($method = Client::ACCOUNT_BALANCE)
    {
    	$client = new \artdevision\bittrex\Client('2a7f2e4e4ad24cd09ca2f314bfb043fe', '55d2180c13c041d5acccbe332246611a');
    	$response = $client->getPublic(\artdevision\bittrex\Client::PUBLIC_CURRENCIES);

    	if ((int) $response->getStatusCode() == 200) {
    		$res = \yii\helpers\Json::decode((string) $response->getBody());
    		if ($res['success'] == true) {
// 	    		$transaction = \Yii::$app->db->beginTransaction();

	    		foreach ($res['result'] as $item) {
	    			print_r($item);
					$model = new \app\models\BitCurrency();
					$model->name = $item['Currency'];
					$model->full_name = $item['CurrencyLong'];
					$model->save(false);
	    		}
// 	    		$transaction->commit();
    		}
//     		echo \yii\helpers\VarDumper::dumpAsString($res);
    		echo \yii\helpers\VarDumper::dumpAsString($response->curl_info);
    	}

//         echo $message . "\n";
    }

    public function actionSummaries() {
    	while(true) {
    		sleep(1);
	    	$client = new \artdevision\bittrex\Client('2a7f2e4e4ad24cd09ca2f314bfb043fe', '55d2180c13c041d5acccbe332246611a');
	    	$response = $client->getPublic(\artdevision\bittrex\Client::PUBLIC_MARKETSUMMARIES);
	    	if ((int) $response->getStatusCode() == 200) {
	    		$res = \yii\helpers\Json::decode((string) $response->getBody());
	    		if ($res['success'] == true) {
	    			$transaction = \Yii::$app->db->beginTransaction();
	    			foreach ($res['result'] as $item) {
	    				\app\models\BitMarket::updateAll([
	    					'last' => $item['Last'],
	    					'bid' => $item['Bid'],
	    					'ask' => $item['Ask'],
	    					'volume' => $item['Volume'],
	    					'buy_orders' => $item['OpenBuyOrders'],
	    					'sell_orders' => $item['OpenSellOrders']
	    				], ['and',
	    						['=', 'name', $item['MarketName']],
	    						['=', 'exchange_id', 1]
	    				]);
	    			}
	    			$transaction->commit();
	    		}
	    	}
    	}
    }

    public function actionMarkets($method = Client::ACCOUNT_BALANCE)
    {
    	$client = new \artdevision\bittrex\Client('2a7f2e4e4ad24cd09ca2f314bfb043fe', '55d2180c13c041d5acccbe332246611a');
    	$response = $client->getPublic(\artdevision\bittrex\Client::PUBLIC_MARKETS);

    	if ((int) $response->getStatusCode() == 200) {
    		$res = \yii\helpers\Json::decode((string) $response->getBody());
    		if ($res['success'] == true) {
    			// 	    		$transaction = \Yii::$app->db->beginTransaction();

    			foreach ($res['result'] as $item) {
    				print_r($item);
    				$model = new \app\models\BitMarket();
    				$model->setAttributes([
						'name' => $item['MarketName'],
    					'base_currency' => $item['BaseCurrency'],
    					'market_currency' => $item['MarketCurrency'],
    					'active' => $item['IsActive'],
    					'min_trade_size' => $item['MinTradeSize'],
    					'exchange_id' => 1,
    				]);
    				$model->save(false);
    			}
    			// 	    		$transaction->commit();
    		}
    		//     		echo \yii\helpers\VarDumper::dumpAsString($res);
    		echo \yii\helpers\VarDumper::dumpAsString($response->curl_info);
    	}

    	//         echo $message . "\n";
    }

    public function actionOrders() {
    	$client = new \artdevision\bittrex\Client('2a7f2e4e4ad24cd09ca2f314bfb043fe', '55d2180c13c041d5acccbe332246611a');
    	$response = $client->getAccount(\artdevision\bittrex\Client::ACCOUNT_ORDERHISTORY);
    	if ((int) $response->getStatusCode() == 200) {
    		$res = \yii\helpers\Json::decode((string) $response->getBody());
    		print_r($res);
    		echo \yii\helpers\VarDumper::dumpAsString($response->curl_info);
    	}
    }
}
