<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%currency}}".
 *
 * @property string $name
 * @property string $full_name
 *
 * @property Market[] $markets
 * @property Market[] $markets0
 */
class BitCurrency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50],
            [['full_name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'full_name' => 'Full Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarkets()
    {
        return $this->hasMany(Market::className(), ['market_currency' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarkets0()
    {
        return $this->hasMany(Market::className(), ['base_currency' => 'name']);
    }


    public function beforeSave($insert) {
    	if (parent::beforeSave($insert)) {
    		if ($insert)
    			$this->created_at = new \yii\db\Expression('NOW()');
    		$this->updated_at = new \yii\db\Expression('NOW()');
    		return true;
    	}
    }
}
