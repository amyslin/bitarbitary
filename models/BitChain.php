<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%chain}}".
 *
 * @property int $id
 * @property string $name
 * @property string $base_currency
 * @property int $exchange_id
 *
 * @property Exchange $exchange
 * @property ChainItem[] $chainItems
 */
class BitChain extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%chain}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exchange_id'], 'integer'],
            [['name', 'base_currency'], 'string', 'max' => 50],
            [['exchange_id'], 'exist', 'skipOnError' => true, 'targetClass' => Exchange::className(), 'targetAttribute' => ['exchange_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'base_currency' => 'Base Currency',
            'exchange_id' => 'Exchange ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExchange()
    {
        return $this->hasOne(BitExchange::className(), ['id' => 'exchange_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChainItems()
    {
        return $this->hasMany(BitChainItem::className(), ['chain_id' => 'id']);
    }
}
