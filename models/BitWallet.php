<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%wallet}}".
 *
 * @property int $id
 * @property string $symbol
 * @property int $exchange_id
 * @property string $balance
 *
 * @property Exchange $exchange
 * @property Currency $symbol0
 */
class BitWallet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%wallet}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exchange_id'], 'integer'],
            [['balance'], 'number'],
            [['symbol'], 'string', 'max' => 50],
            [['symbol', 'exchange_id'], 'unique', 'targetAttribute' => ['symbol', 'exchange_id']],
            [['exchange_id'], 'exist', 'skipOnError' => true, 'targetClass' => BitExchange::className(), 'targetAttribute' => ['exchange_id' => 'id']],
            [['symbol'], 'exist', 'skipOnError' => true, 'targetClass' => BitCurrency::className(), 'targetAttribute' => ['symbol' => 'name']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'symbol' => 'Symbol',
            'exchange_id' => 'Exchange ID',
            'balance' => 'Balance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExchange()
    {
        return $this->hasOne(BitExchange::className(), ['id' => 'exchange_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(BitCurrency::className(), ['name' => 'symbol']);
    }
    
    public static function updateHistory() {
$sql = <<<SQL
SELECT 
  w.id,
  w.balance, 
  CASE WHEN w.symbol = 'USDT' THEN  w.balance ELSE w.balance * bm.last END AS price_usd,
  CASE 
    WHEN w.symbol = 'BTC' 
    THEN  w.balance 
    WHEN w.symbol = 'USDT'
    THEN w.balance / btm.last
    ELSE w.balance * bbm.last 
  END AS price_btc,
    CASE 
    WHEN w.symbol = 'ETH' 
    THEN  w.balance 
    WHEN w.symbol = 'USDT'
    THEN w.balance / etm.last
    ELSE w.balance * ebm.last 
  END AS price_eth
  FROM bit_wallet w 
  LEFT JOIN bit_market bm ON bm.base_currency = 'USDT' AND bm.market_currency = w.symbol
  LEFT JOIN bit_market btm ON btm.base_currency = 'USDT' AND btm.market_currency = 'BTC'
  LEFT JOIN bit_market bbm ON bbm.base_currency = 'BTC' AND bbm.market_currency = w.symbol
  LEFT JOIN bit_market etm ON etm.base_currency = 'USDT' AND etm.market_currency = 'ETH'
  LEFT JOIN bit_market ebm ON ebm.base_currency = 'ETH' AND etm.market_currency = w.symbol;
SQL;

		$data = \Yii::$app->db->createCommand($sql)->queryAll();

		$sql = \Yii::$app->db->queryBuilder->batchInsert('bit_balance_hystory',
				['wallet_id', 'balance', 'price_usd', 'price_btc', 'price_eth'], $data);
		$sql = str_replace('INSERT INTO', 'INSERT IGNORE', $sql);
		\Yii::$app->db->createCommand($sql)->execute();
    }
    
    public static function updateSummaries() {
$sql = <<<SQL
SELECT bw.symbol, 
  bw.exchange_id,
  CASE 
    WHEN bw.symbol = 'USDT' THEN a.balance_usd
    WHEN bw.symbol = 'BTC' THEN a.balance_btc
    ELSE NULL
  END AS value
  FROM bit_wallet bw
LEFT JOIN (SELECT 
  w.*, 
  SUM(CASE WHEN w.symbol = 'USDT' THEN  w.balance ELSE w.balance * bm.last END) AS balance_usd,
  SUM(CASE 
    WHEN w.symbol = 'BTC' 
    THEN  w.balance 
    WHEN w.symbol = 'USDT'
    THEN w.balance / btm.last
    ELSE w.balance * bbm.last 
  END) AS balance_btc
  FROM bit_wallet w 
  LEFT JOIN bit_market bm ON bm.base_currency = 'USDT' AND bm.market_currency = w.symbol
  LEFT JOIN bit_market btm ON btm.base_currency = 'USDT' AND btm.market_currency = 'BTC'
  LEFT JOIN bit_market bbm ON bbm.base_currency = 'BTC' AND bbm.market_currency = w.symbol
  ) a ON a.exchange_id = bw.exchange_id WHERE bw.symbol IN ('USDT', 'BTC', 'ETH');
SQL;
		$data = \Yii::$app->db->createCommand($sql)->queryAll();
		
		$sql = \Yii::$app->db->queryBuilder->batchInsert('bit_balance_summaries',
				['symbol', 'exchange_id', 'value'], $data);
		$sql = str_replace('INSERT INTO', 'INSERT IGNORE', $sql);
		\Yii::$app->db->createCommand($sql)->execute();

    }
}
