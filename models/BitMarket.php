<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%market}}".
 *
 * @property int $id
 * @property string $name
 * @property string $base_currency
 * @property string $market_currency
 * @property string $min_trade_size
 * @property int $active
 * @property string $created_at
 * @property string $updated_at
 * @property int $exchange_id
 *
 * @property ChainItem[] $chainItems
 * @property Currency $marketCurrency
 * @property Currency $baseCurrency
 * @property Exchange $exchange
 */
class BitMarket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%market}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['min_trade_size'], 'number'],
            [['active', 'exchange_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['base_currency', 'market_currency'], 'string', 'max' => 10],
            [['name', 'exchange_id'], 'unique', 'targetAttribute' => ['name', 'exchange_id']],
            [['market_currency'], 'exist', 'skipOnError' => true, 'targetClass' => BitCurrency::className(), 'targetAttribute' => ['market_currency' => 'name']],
            [['base_currency'], 'exist', 'skipOnError' => true, 'targetClass' => BitCurrency::className(), 'targetAttribute' => ['base_currency' => 'name']],
            [['exchange_id'], 'exist', 'skipOnError' => true, 'targetClass' => BitExchange::className(), 'targetAttribute' => ['exchange_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'base_currency' => 'Base Currency',
            'market_currency' => 'Market Currency',
            'min_trade_size' => 'Min Trade Size',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'exchange_id' => 'Exchange ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChainItems()
    {
        return $this->hasMany(BitChainItem::className(), ['market_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarketCurrency()
    {
        return $this->hasOne(BitCurrency::className(), ['name' => 'market_currency']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBaseCurrency()
    {
        return $this->hasOne(BitCurrency::className(), ['name' => 'base_currency']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExchange()
    {
        return $this->hasOne(Exchange::className(), ['id' => 'exchange_id']);
    }


    public function beforeSave($insert) {
    	if (parent::beforeSave($insert)) {
    		if ($insert)
    			$this->created_at = new \yii\db\Expression('NOW()');
    		$this->updated_at = new \yii\db\Expression('NOW()');
    		return true;
    	}
    }
}
