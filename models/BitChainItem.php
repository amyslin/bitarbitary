<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%chain_item}}".
 *
 * @property int $id
 * @property string $opreration
 * @property int $market_id
 * @property int $chain_id
 *
 * @property Chain $chain
 * @property Market $market
 */
class BitChainItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%chain_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['opreration'], 'string'],
            [['market_id', 'chain_id'], 'integer'],
            [['chain_id'], 'exist', 'skipOnError' => true, 'targetClass' => Chain::className(), 'targetAttribute' => ['chain_id' => 'id']],
            [['market_id'], 'exist', 'skipOnError' => true, 'targetClass' => Market::className(), 'targetAttribute' => ['market_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'opreration' => 'Opreration',
            'market_id' => 'Market ID',
            'chain_id' => 'Chain ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChain()
    {
        return $this->hasOne(BitChain::className(), ['id' => 'chain_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarket()
    {
        return $this->hasOne(BitMarket::className(), ['id' => 'market_id']);
    }
}
