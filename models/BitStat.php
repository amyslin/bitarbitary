<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%signals_stat}}".
 *
 * @property int $id
 * @property string $created_at
 * @property int $chain_id
 * @property string $amount_in
 * @property string $amount_out
 * @property string $profit
 * @property string $m1_last
 * @property string $m2_last
 * @property string $m3_last
 *
 * @property Chain $chain
 */
class BitStat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%signals_stat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['chain_id'], 'integer'],
            [['amount_in', 'amount_out', 'profit', 'm1_last', 'm2_last', 'm3_last'], 'number'],
            [['chain_id'], 'exist', 'skipOnError' => true, 'targetClass' => BitChain::className(), 'targetAttribute' => ['chain_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'chain_id' => 'Chain ID',
            'amount_in' => 'Amount In',
            'amount_out' => 'Amount Out',
            'profit' => 'Profit',
            'm1_last' => 'M1 Last',
            'm2_last' => 'M2 Last',
            'm3_last' => 'M3 Last',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChain()
    {
        return $this->hasOne(BitChain::className(), ['id' => 'chain_id']);
    }
}
