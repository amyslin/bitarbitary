<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
global $amount;
global $rowResult;
$rowResult = [];

$amount = $dealAmount;
?>
<div class="body-content">
<?= \yii\grid\GridView::widget([
		'dataProvider' => $provider,
		'columns' => [
			'name',
			'base_currency',
			[
				'label' => 'Сумма',
				'format' => 'raw',
				'value' => function ($data) {
					global $amount;
					return $amount;
				},
			],
			[
				'label' => 'Шаг 1',
				'format' => 'raw',
				'value' => function ($data) {
					global $amount;
					return Html::tag('span', $data['chainItems'][0]['operation']);

// 					return print_r($amount, true);
				},

			],
			[
				'label' => 'Шаг 1',
				'format' => 'raw',
				'value' => function ($data, $num) {
					global $amount;
					global $rowResult;

					$rowResult[$num][] = $amount / $data['chainItems'][0]['market']['last'];
					return Html::tag('span', $amount . '->' . $rowResult[$num][0]);

					$client = new \artdevision\bittrex\Client('2a7f2e4e4ad24cd09ca2f314bfb043fe', '55d2180c13c041d5acccbe332246611a');
					$response = $client->getPublic(\artdevision\bittrex\Client::PUBLIC_TICKER, ['market' => $data['chainItems'][0]['market']['name']]);
					if ((int) $response->getStatusCode() == 200) {
						$res = \yii\helpers\Json::decode((string) $response->getBody());
						$rowResult[$num][] = $amount / $res['result']['Last'];

						return Html::tag('span', $amount . '->' . $amount / $res['result']['Last']);
					}
					else
						return null;

					// 					return print_r($amount, true);
				},

			],
			[
				'label' => 'Шаг 2',
				'format' => 'raw',
				'value' => function ($data, $num) {
					global $amount;
					global $rowResult;

					$rowResult[$num][] = $rowResult[$num][0] / $data['chainItems'][1]['market']['last'];
					return Html::tag('span', $rowResult[$num][0] . '->' . $rowResult[$num][1]);

					$client = new \artdevision\bittrex\Client('2a7f2e4e4ad24cd09ca2f314bfb043fe', '55d2180c13c041d5acccbe332246611a');
					$response = $client->getPublic(\artdevision\bittrex\Client::PUBLIC_TICKER, ['market' => $data['chainItems'][1]['market']['name']]);
					if ((int) $response->getStatusCode() == 200) {
						$res = \yii\helpers\Json::decode((string) $response->getBody());
						$rowResult[$num][] = $rowResult[$num][0] / $res['result']['Last'];

						return Html::tag('span', $rowResult[$num][0] . '->' . $rowResult[$num][0] / $res['result']['Last']);
					}
					else
						return null;

					// 					return print_r($amount, true);
				},

			],
			[
				'label' => 'Шаг 2',
				'format' => 'raw',
				'value' => function ($data, $num) {
					global $amount;
					global $rowResult;

					$rowResult[$num][] = $rowResult[$num][1] * $data['chainItems'][2]['market']['last'];
					return Html::tag('span', $rowResult[$num][1] . '->' . $rowResult[$num][2]);

					$client = new \artdevision\bittrex\Client('2a7f2e4e4ad24cd09ca2f314bfb043fe', '55d2180c13c041d5acccbe332246611a');
					$response = $client->getPublic(\artdevision\bittrex\Client::PUBLIC_TICKER, ['market' => $data['chainItems'][2]['market']['name']]);
					if ((int) $response->getStatusCode() == 200) {
						$res = \yii\helpers\Json::decode((string) $response->getBody());
						$rowResult[$num][] = $rowResult[$num][1] * $res['result']['Last'];

						return Html::tag('span', $rowResult[$num][1] . '->' . $rowResult[$num][1] * $res['result']['Last']);
					}
					else
						return null;

					// 					return print_r($amount, true);
				},

			],
			[
				'label' => 'Профит $',
				'format' => 'raw',
				'value' => function ($data, $num) {
					global $amount;
					global $rowResult;
					$valueProfit = $rowResult[$num][2] - $amount;
					return $valueProfit;

					// 					return print_r($amount, true);
				},
			],
			[
				'label' => 'Профит $',
				'format' => 'raw',
				'value' => function ($data, $num) {
					global $amount;
					global $rowResult;
					$valueProfit = $rowResult[$num][2] - $amount;

					return (100/($amount/$valueProfit)) . '%';

					// 					return print_r($amount, true);
					},
			],
		]
	]);
?>
</div>