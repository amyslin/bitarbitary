<?php
namespace app\db;

class UpdateCommand extends \yii\db\mysql\QueryBuilder {
	public function batchInsert($table, $columns, $rows) {
		$sql = parent::batchInsert($table, $columns, $rows);
		$sql .= 'ON DUPLICATE KEY UPDATE';
		return $sql;
	}
}
